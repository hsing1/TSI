var xhr = null;

function createXHR(){	
	//alert("createXHR");
	if( window.XMLHttpRequest ){	
		xhr = new XMLHttpRequest();
	}else if( window.ActiveXObject ){		
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	} else
		xhr = null;    
	
	return xhr;
}	

function feedback() {
	$("#feedback").css("display", "block");
}

function getContact(){	
	xhr1 = createXHR();
	//alert("getTown");
	if( xhr1 == null ){
		alert("no xhr creted");
		return;
	}		   
	  
	var message = document.getElementById("message");
	var category = document.getElementById("category");
	var name = document.getElementById("name");
	var title = document.getElementById("title");
	var company = document.getElementById("company");
	var email = document.getElementById("email");
	var phone = document.getElementById("phone");
	var fax = document.getElementById("fax");
	var address = document.getElementById("address");
	var subject= "Contact From Response";
	//var recipient = "hsingyichu@gmail.com";
	var recipient = "info@tsi.com.au";
	
	
	//var deptId = selectDept.value;
	var catId = category.selectedIndex;
	//var deptId = $('#selectDept1').value();
	var url = 'http://mailgate.server-mail.com/cgi-bin/mailgate';
	var requestData = 'name=' + name + "&title=" + title + "&category=" + category + "&message=" + message
						+ "&company=" + company + "&email=" + email + "&phone=" + phone + "&fax=" + fax + "&address=" + address
						+ "&subject=" + subject + "&recipient=" + recipient;
	xhr1.open('POST', url, true);
	xhr1.onreadystatechange = feedback;
	xhr1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr1.send(requestData);
	
	return false;
}