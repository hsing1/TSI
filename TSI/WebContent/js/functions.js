$(function() {
	// This will disable the links. Remove it.
	/*
	$('a').on('click', function(e) {
		if ($(this).attr('href') == '#') { 
			e.preventDefault();
		}
	});
	*/
	
	$('#header-featured-img').jCarouselLite({
		btnNext: '.md-next-button',
		btnPrev: '.md-prev-button',
		visible: 1,
		auto: 9000,
		speed: 1000
	});
});
